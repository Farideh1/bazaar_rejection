import React from 'react';
import './Reason.css'

function Reason (props) {
  return(
   <ul className="reasons">
     {props.reasons.map(reason =>
       <li key={reason.id} className="reasons__node">
         {/*if Im right; it either has description or children according to the task*/}
         {reason.description &&
           <div className="reasons__node__withDescription">
             <span>{reason.name}</span>
             <input type="checkbox"
                    className="reasons__node__withDescription__checkbox"
                    id={reason.id}
                    onChange={() => props.handleAddingDescription(reason.description, reason.id)}/>
           </div>
         }
         {reason.children &&
           <div className="reasons__node__withChildren">
             <input type="checkbox" className="reasons__node__withChildren__checkbox" id={reason.id}/>
             <label htmlFor={reason.id}>{reason.name}</label>
             {/* recursive call if there are nested children */}
             <Reason reasons={reason.children}
                     handleAddingDescription={props.handleAddingDescription} />
           </div>
         }
       </li>)
     }
    </ul>
  )
}

export default Reason;
