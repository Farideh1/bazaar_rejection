import React, {Component} from 'react';
import axios from 'axios';
import Loader from '../../Components/Loader/Loader'
import Reason from '../../Components/Reason/Reason'
import './Panel.css';

class Panel extends Component{
  state = {
    loading: true,
    reasons: [],
    queryResults: [],
    searchQuery: '',
    email: {
      descriptionArray: [], // storing descriptions in array so we could add/remove them on checkbox toggle
      descriptionsText: '',
      bodyText: '' // email body that user enters
    }
  }
  componentDidMount(){
    axios({
      method: 'GET',
      url: 'https://gist.githubusercontent.com/arash16/00d89a8b84b4ede9a438c16886a6ec09/raw/b7858359943cbc511bcc16c84609ab28d029b283/rejection-reasons.json',
    }).then(response => {
      this.setState({reasons: response.data, queryResults: response.data, loading: false});
    }).catch(error => {
      this.setState({loading: false})
    })
  }
  handleAddingDescription = (body, id) => {
    this.setState(state => {
      const descriptionArray = [...state.email.descriptionArray];

      // toggle description
      const descIndex = descriptionArray.findIndex(desc => desc.id === id)
      descIndex !== -1 ? descriptionArray.splice(descIndex,1) : descriptionArray.push({id: id, body: body});

      // showing descriptions text
      const descriptionsText = descriptionArray.reduce((descriptions,elem) => {
        descriptions.push(elem.body);
        return descriptions
      },[]);

      return {email: {...state.email, descriptionArray, descriptionsText: descriptionsText.join("\n\n") }}
    });
  }
  handleEmailBodyChange = (event) => {
    const bodyText = event.target.value;
    this.setState(state => {
      return { email: {...state.email, bodyText}}
    });
  }
  handleFilterReasons = (event) => {
    const searchQuery = event.target.value;
    this.setState( state => {
      const queryResults = state.reasons.filter(reason =>
       (reason.name.includes(searchQuery) || (reason.description && reason.description.includes(searchQuery)))
      )
      return {searchQuery, queryResults }
    })
  }

  render(){
    const {loading, email, searchQuery, queryResults} = this.state;
    return(
     <div className="container">
       <div className="container__reasons">
         <h3>دلایل رد</h3>
         <input placeholder="جستجو در دلایل رد"
                value={searchQuery}
                onChange={this.handleFilterReasons}/>
         <div>
           {loading && <Loader/>}
           <Reason reasons={queryResults}
                 handleAddingDescription={this.handleAddingDescription}/>
         </div>
       </div>
       <div className="container__email">
         <h3>ایمیل به خالق</h3>
         <p>متن ایمیل</p>
         <div className="container__email__body">
           { email.descriptionsText && <div>{email.descriptionsText}</div>}
           <textarea rows={4}
                     placeholder="متن مورد نظر خود را وارد کنید"
                     value={email.bodyText}
                     onChange={this.handleEmailBodyChange}/>
         </div>
       </div>
     </div>
    )
  }
}

export default Panel
