import React from 'react';
import ReactDOM from 'react-dom';
import Panel from './Containers/Panel/Panel';
import './index.css'

ReactDOM.render(<Panel />, document.getElementById('root'));
